### Install project

1. Starting docker-compose.yml
2. In "php" container starting
3. ```composer create-project laravel/laravel:^11.0 .```
4. ```chmod -R 775 storage && chown -R $USER:www-data storage```
5. ```php artisan migrate```
