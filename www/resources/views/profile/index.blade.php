@extends('layouts.layout')
@section('title', 'profile')
@section('content')
    @if (session('success'))
        {{ session('success') }}
    @endif

    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    Обновление основных данных
                </h1>
            </div>

            <form action="{{ route('profile.update') }}" method="POST">
                @csrf
                @method('patch')

                <div
                    class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">
                    <div class="relative flex-grow w-full">
                        <label for="full-name" class="leading-7 text-sm text-gray-600">Имя</label>
                        <input type="text"
                               id="name"
                               name="name"
                               value="{{$user->name}}"
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>

                    <div class="relative flex-grow w-full">
                        <label for="email" class="leading-7 text-sm text-gray-600">Почта</label>
                        <input type="email"
                               id="email"
                               name="email"
                               value="{{$user->email}}"
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>

                    <button
                        class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                        Обновить
                    </button>
                </div>
            </form>
        </div>
    </section>

    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    Обновление пароля
                </h1>
            </div>

            <form action="{{ route('profile.update.password') }}" method="POST">
                @csrf
                @method('patch')

                <div
                    class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">
                    <div class="relative flex-grow w-full">
                        <label for="old_password" class="leading-7 text-sm text-gray-600">Старый пароль</label>
                        <input type="text"
                               id="old_password"
                               name="old_password"
                               value=""
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>

                    <div class="relative flex-grow w-full">
                        <label for="new_password" class="leading-7 text-sm text-gray-600">Новый пароль</label>
                        <input type="text"
                               id="new_password"
                               name="new_password"
                               value=""
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>

                    <button
                        class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                        Обновить
                    </button>
                </div>
            </form>
        </div>
    </section>
@endsection
