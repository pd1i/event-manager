<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | EventManager</title>
    <!-- Styles -->
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
<div class="container-page">
    @include('layouts.header')

    @yield('content')
</div>
</body>
</html>
