@extends('layouts.layout')
@section('title', 'events')
@section('content')
    @if (session('success'))
        {{ session('success') }}
    @endif

    {{-- Информационный раздел --}}
    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    Event name: {{ $event->name }}
                </h1>
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
                    Event title: {{ $event->title }}
                </h1>
            </div>
        </div>
    </section>

    {{-- Создание пригласительной ссылки --}}
    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <form method="POST" action="{{ route('shares.create', $event->id) }}">
                    @csrf

                    <div
                        class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">

                        <div class="relative flex-grow w-full pt-2">
                            <button
                                class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                                Поделиться
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    {{-- Добавление продукта --}}
    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    Добавление элемента
                </h1>
            </div>

            <form action="{{ route('products.create', $event->id) }}" method="POST">
                @csrf

                <div
                    class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">
                    <div class="relative flex-grow w-full">
                        <label for="full-name" class="leading-7 text-sm text-gray-600">Название элемента</label>
                        <input type="text" id="name" name="name"
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>
                    <div class="relative flex-grow w-full">
                        <label for="email" class="leading-7 text-sm text-gray-600">Количество элементов</label>
                        <input type="number" id="count" name="count"
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>
                    <div class="relative flex-grow w-full">
                        <label for="email" class="leading-7 text-sm text-gray-600">Цена элемента</label>
                        <input type="number" id="price" name="price"
                               class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                    </div>
                    <button
                        class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                        Сохранить
                    </button>
                </div>
            </form>
        </div>
    </section>

    {{-- Фильтрация по продуктам --}}
    <section class="text-gray-600 body-font overflow-hidden">
        <div class="container px-1 py-4 mx-auto">
            <div class="lg:w-4/5 mx-auto flex flex-wrap">
                <div class="lg:w-1/2 w-full lg:pl-10 lg:py-1 mt-1 lg:mt-0">
                    <div class="flex mt-6 items-center pb-5 border-b-2 border-gray-100 mb-5">
                        <div class="flex ml-6 items-center">
                            <span class="mr-3">Filter users</span>
                            <div class="relative">
                                <form>
                                    <select
                                        name="user_id"
                                        class="rounded border appearance-none border-gray-300 py-2 focus:outline-none
                                         focus:ring-2 focus:ring-indigo-200 focus:border-indigo-500 text-base pl-3 pr-10">
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>

                                    <br>

                                    <input type="number"
                                           id="count_min"
                                           name="count_min"
                                           value=""
                                           class="rounded border appearance-none border-gray-300 py-2 focus:outline-none
                                         focus:ring-2 focus:ring-indigo-200 focus:border-indigo-500 text-base pl-3 pr-10">

                                    <br>

                                    <input type="number"
                                           id="count_max"
                                           name="count_max"
                                           value=""
                                           class="rounded border appearance-none border-gray-300 py-2 focus:outline-none
                                         focus:ring-2 focus:ring-indigo-200 focus:border-indigo-500 text-base pl-3 pr-10">

                                    <br>

                                    <button class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg"
                                        type="submit">Фильтровать</button>
                                </form>
                                <span
                                    class="absolute right-0 top-0 h-full w-10 text-center text-gray-600 pointer-events-none flex items-center justify-center">
                                    <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                         stroke-linejoin="round" stroke-width="2" class="w-4 h-4" viewBox="0 0 24 24">
                                      <path d="M6 9l6 6 6-6"></path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="text-gray-600 body-font">
        <div class="container px-5 py-3 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    Список элементов
                </h1>
            </div>

            <div class="">
                @foreach ($products as $product)
                    <form action="{{ route('products.update', $product->id) }}" method="POST">
                        @csrf
                        @method('patch')

                        <div
                            class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">
                            <div class="relative flex-grow w-full">
                                <label for="full-name" class="leading-7 text-sm text-gray-600">Название элемента</label>
                                <input type="text"
                                       id="name"
                                       name="name"
                                       value="{{$product->user->name}}"
                                       class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300
                                       focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200
                                       text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors
                                       duration-200 ease-in-out">
                            </div>
                            <div class="relative flex-grow w-full">
                                <label for="full-name" class="leading-7 text-sm text-gray-600">Название элемента</label>
                                <input type="text"
                                       id="name"
                                       name="name"
                                       value="{{$product->name}}"
                                       class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300
                                       focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200
                                       text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors
                                       duration-200 ease-in-out">
                            </div>
                            <div class="relative flex-grow w-full">
                                <label for="email" class="leading-7 text-sm text-gray-600">Количество элементов</label>
                                <input type="number"
                                       id="count"
                                       name="count"
                                       value="{{$product->count}}"
                                       class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300
                                       focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200
                                       text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors
                                       duration-200 ease-in-out">
                            </div>
                            <div class="relative flex-grow w-full">
                                <label for="email" class="leading-7 text-sm text-gray-600">Цена элемента</label>
                                <input type="number"
                                       id="price"
                                       name="price"
                                       value="{{$product->price}}"
                                       class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300
                                       focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200
                                       text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors
                                       duration-200 ease-in-out">
                            </div>
                            <button
                                class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                                Изменить
                            </button>
                        </div>
                    </form>

                    <form method="POST" action="{{ route('products.delete', $product->id) }}">
                        @csrf
                        @method('delete')

                        <div
                            class="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:space-x-4 sm:space-y-0 space-y-4 sm:px-0 items-end">

                            <div class="relative flex-grow w-full pt-2">
                                <button
                                    class="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                                    Удалить
                                </button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>
    </section>

    <section class="text-gray-600 body-font">
        <div class="container px-5 py-24 mx-auto">
            <div class="flex flex-wrap w-full mb-20 flex-col items-center text-center">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
                    Список пользователей
                </h1>
            </div>
            <div class="flex flex-wrap -m-4">
                @foreach ($users as $user)
                    <div class="xl:w-1/3 md:w-1/2 p-4">
                        <div class="border border-gray-200 p-6 rounded-lg">
                            <h2 class="text-lg text-gray-900 font-medium title-font mb-2">
                                {{ $user->name }}
                            </h2>
                            <p class="leading-relaxed text-base">
                                Lorem ipsum dolor.
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
