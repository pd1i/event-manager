@extends('layouts.layout')
@section('title', 'sign-in')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif

    <form action="{{ route('sign.in.create') }}" class="max-w-sm mx-auto" method="POST">
        @csrf

        {{-- email input --}}
        <div class="mb-5">
            <label for="email" class="leading-7 text-sm text-gray-600">
                Ваша почта
            </label>
            <input type="email"
                   id="email"
                   name="email"
                   class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                   placeholder="email"
                   required
            />
        </div>

        {{-- password input --}}
        <div class="mb-5">
            <label for="password" class="leading-7 text-sm text-gray-600">
                Ваш пароль
            </label>
            <input type="password"
                   id="password"
                   name="password"
                   class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                   required
            />
        </div>

        {{-- remember checkbox --}}
        <div class="flex items-start mb-5">
            <div class="flex items-center h-5">
                <input id="remember"
                       name="remember"
                       type="checkbox"
                       value=""
                       class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                />
            </div>
            <label for="remember" class="ms-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Сохранить вход?
            </label>
        </div>

        {{-- submit button --}}
        <button type="submit"
                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Выполнить авторизацию
        </button>
    </form>
@endsection



