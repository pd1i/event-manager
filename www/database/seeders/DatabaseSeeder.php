<?php

namespace Database\Seeders;

use App\Models\Attribute;
use App\Models\AttributeType;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use App\Models\Event;
use App\Models\EventUser;

use Database\Factories\AttributeTypeFactory;
use Illuminate\Database\Seeder;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        /*User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@root.com'
        ]);
        User::factory()->create([
            'name' => 'testing',
            'email' => 'testing@root.com'
        ]);*/

        /*Role::factory()->create([
            'name' => 'Creator',
            'description' => 'Creator in event'
        ]);
        Role::factory()->create([
            'name' => 'user',
            'description' => 'User'
        ]);*/

        Event::factory(5)->create();
        EventUser::factory(5)->create();
        //Product::factory(5)->create();

        /*AttributeType::factory()->create([
            'name' => 'Строка'
        ]);
        AttributeType::factory()->create([
            'name' => 'Список'
        ]);*/
    }
}
