<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProfileTest extends TestCase
{
    /**
     * Проверка на отображение страницы c профилем
     */
    public function testViewPage()
    {
        $this->post('/sign-in', [
            'email' => 'admin@root.com',
            'password' => 'password',
        ]);

        $response = $this->get('/profile');
        $response->assertOk();
    }
}
