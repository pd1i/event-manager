<?php

namespace Tests\Feature;

use Tests\TestCase;

class SignInTest extends TestCase
{
    /**
     * Проверка на отображение страницы с авторизацией
     * @return void
     */
    public function testViewPage(): void
    {
        $response = $this->get('/sign-in');

        $response->assertOk();
    }

    /**
     * Проверка на отображение страницы с авторизацией
     */
    public function testSignInLogic()
    {
        $this->post('/sign-in', [
            'email' => 'admin@root.com',
            'password' => 'password',
        ]);

        $response = $this->get('/events');
        $response->assertOk();
    }
}
