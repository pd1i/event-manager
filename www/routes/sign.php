<?php

use App\Http\Controllers\Sign\SignController;
use App\Http\Controllers\Sign\SignInController;
use App\Http\Controllers\Sign\SignUpController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    /**
     * Обработка регистрации
     */
    Route::get('/sign-up', [SignUpController::class, 'index']);
    Route::post('/sign-up', [SignUpController::class, 'signUp'])->name('sign.up.create');

    /**
     * Обработка авторизации
     */
    Route::get('/sign-in', [SignInController::class, 'index']);
    Route::post('/sign-in', [SignInController::class, 'signIn'])->name('sign.in.create');
});

Route::middleware('auth')->group(function () {
    /**
     * Обработка выхода
     */
    Route::post('/logout', [SignController::class, 'logout'])->name('logout');
});
