<?php

declare(strict_types=1);

use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Product\Attribute\AttributeCreateScreen;
use App\Orchid\Screens\Product\Attribute\AttributeListLayout;
use App\Orchid\Screens\Product\Attribute\AttributeListScreen;
use App\Orchid\Screens\Product\ProductCreateScreen;
use App\Orchid\Screens\Product\ProductEditScreen;
use App\Orchid\Screens\Product\ProductListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

/**
 * Отображение главной страницы
 */
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

/**
 * Отображение профиля
 */
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Profile'), route('platform.profile')));

/**
 * Редактирование пользователя
 */
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(fn(Trail $trail, $user) => $trail
        ->parent('platform.systems.users')
        ->push($user->name, route('platform.systems.users.edit', $user)));

/**
 * Создание пользователя
 */
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.systems.users')
        ->push(__('Create'), route('platform.systems.users.create')));

/**
 * Список пользователей
 */
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Users'), route('platform.systems.users')));

/**
 * Редактирование роли
 */
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(fn(Trail $trail, $role) => $trail
        ->parent('platform.systems.roles')
        ->push($role->name, route('platform.systems.roles.edit', $role)));

/**
 * Создание роли
 */
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.systems.roles')
        ->push(__('Create'), route('platform.systems.roles.create')));

/**
 * Список ролей
 */
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Roles'), route('platform.systems.roles')));

/**
 * Отображение продуктов
 */
Route::screen('products', ProductListScreen::class)
    ->name('platform.products')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Products'), route('platform.products')));

/**
 * Детальная страница продукта
 */
Route::screen('products/{product}', ProductEditScreen::class)
    ->name('platform.products.edit')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.products')
        ->push(__('Edit'), route('platform.products')));

/**
 * Создание продукта
 */
Route::screen('product/create', ProductCreateScreen::class)
    ->name('platform.products.create')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.products')
        ->push(__('Create'), route('platform.products')));

/**
 * Список свойств продукта
 */
Route::screen('product/attributes', AttributeListScreen::class)
    ->name('platform.products.attributes')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Products Attributes'), route('platform.products.attributes')));

/**
 * Создание аттрибута продукта
 */
Route::screen('product/attributes/create', AttributeCreateScreen::class)
    ->name('platform.products.attributes.create')
    ->breadcrumbs(fn(Trail $trail) => $trail
        ->parent('platform.index')
        ->push(__('Products Attributes Create'), route('platform.products.attributes.create')));
