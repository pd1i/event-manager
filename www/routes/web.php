<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShareController;
use App\Http\Controllers\Sign\SignController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->name('home');

Route::middleware('auth')->group(function () {
    /**
     * Взаимодействие с событиями
     */
    Route::get('/events', [EventController::class, 'index'])->name('events.index');
    Route::get('/events/{id}', [EventController::class, 'detail'])->name('events.detail');
    Route::post('/events', [EventController::class, 'create'])->name('events.create');
    Route::delete('/events/{id}', [EventController::class, 'delete'])->name('events.delete');

    /**
     * Взаимодействие с пригласительной ссылкой
     */
    Route::post('/shares/{eventId}', [ShareController::class, 'create'])->name('shares.create');
    Route::get('/shares/{inviteCode}', [ShareController::class, 'get'])->name('shares.get');

    /**
     * Взаимодействие с продуктами в событиях
     */
    Route::post('/products/{eventId}', [ProductController::class, 'create'])->name('products.create');
    Route::patch('/products/{productId}', [ProductController::class, 'update'])->name('products.update');
    Route::delete('/products/{productId}', [ProductController::class, 'delete'])->name('products.delete');

    /**
     * Взаимодействие с профилем пользователя
     */
    Route::get('/profile', [UserController::class, 'index'])->name('profile.index');
    Route::patch('/profile/update', [UserController::class, 'update'])->name('profile.update');
    Route::patch('/profile/update/password', [SignController::class, 'updatePassword'])->name('profile.update.password');
});

require __DIR__ . '/sign.php';
