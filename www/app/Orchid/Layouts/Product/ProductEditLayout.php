<?php

namespace App\Orchid\Layouts\Product;

use App\Models\Event;
use App\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class ProductEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('product.name')
                ->required()
                ->title('name'),

            Input::make('product.price')
                ->required()
                ->title('price'),

            Input::make('product.count')
                ->required()
                ->title('count'),

            Select::make('product.user_id')
                ->required()
                ->title('user')
                ->fromModel(User::class, 'name', 'id'),

            Select::make('product.event_id')
                ->required()
                ->title('event')
                ->fromModel(Event::class, 'name', 'id'),

            Input::make('product.created_at')
                ->required()
                ->title('created_at'),

            Input::make('product.updated_at')
                ->required()
                ->title('updated_at'),
        ];
    }
}
