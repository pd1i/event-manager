<?php

namespace App\Orchid\Layouts\Product\Attribute;

use App\Models\Attribute;
use App\Models\AttributeType;
use App\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;
use function Termwind\render;

class AttributeCreateLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('attribute.name')
                ->required()
                ->title('Name'),

            Input::make('attribute.description')
                ->required()
                ->title('Description'),

            Select::make('attribute.attribute_type_id')
                ->required()
                ->title('Attribute type')
                ->fromModel(AttributeType::class, 'name', 'id'),
        ];
    }
}
