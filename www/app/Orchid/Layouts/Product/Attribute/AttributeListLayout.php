<?php

namespace App\Orchid\Layouts\Product\Attribute;

use App\Models\Attribute;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class AttributeListLayout extends Table
{
    /**
     * Data source.
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     * @var string
     */
    protected $target = 'attributes';

    /**
     * Get the table cells to be displayed.
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'id')
                ->cantHide()
                ->width('15px')
                ->render(fn(Attribute $attribute) => $attribute->id),

            TD::make('name', 'name')
                ->cantHide()
                ->width('15px')
                ->render(fn(Attribute $attribute) => $attribute->name),

            TD::make('description', 'description')
                ->cantHide()
                ->width('15px')
                ->render(fn(Attribute $attribute) => $attribute->description),

            TD::make('type', 'type')
                ->cantHide()
                ->width('15px')
                ->render(fn(Attribute $attribute) => $attribute->type->name),
        ];
    }
}
