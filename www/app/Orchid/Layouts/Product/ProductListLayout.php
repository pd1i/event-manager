<?php

namespace App\Orchid\Layouts\Product;

use App\Models\Product;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ProductListLayout extends Table
{
    /**
     * Data source.
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     * @var string
     */
    protected $target = 'products';

    /**
     * Get the table cells to be displayed.
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'id')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->id),

            TD::make('name', 'name')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->name),

            TD::make('price', 'price')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->price),

            TD::make('count', 'count')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->count),

            TD::make('user', 'user')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->user->name),

            TD::make('event', 'event')
                ->cantHide()
                ->width('15px')
                ->render(fn(Product $product) => $product->event->name),

            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn(Product $product) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([
                        Link::make(__('Edit'))
                            ->route('platform.products.edit', $product->id)
                            ->icon('bs.pencil'),

                        Button::make(__('Delete'))
                            ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                            ->icon('bs.trash3')
                            ->method('delete', ['id' => $product->id]),
                    ])),
        ];
    }
}
