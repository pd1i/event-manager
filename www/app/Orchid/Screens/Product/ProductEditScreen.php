<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Orchid\Layouts\Product\ProductEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class ProductEditScreen extends Screen
{
    public $product;

    /**
     * Fetch data to be displayed on the screen.
     * @return array
     */
    public function query(Product $product): iterable
    {
        return [
            'product' => $product
        ];
    }

    /**
     * The name of the screen displayed in the header.
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Product' . ' ' . $this->product->name;
    }

    /**
     * The screen's action buttons.
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Save')->method('save'),
            Button::make('Delete')->method('delete'),
        ];
    }

    /**
     * The screen's layout elements.
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            ProductEditLayout::class
        ];
    }

    /**
     * Сохранение нового продукта
     * @param Request $request
     * @param Product $product
     * @return void
     */
    public function save(Request $request, Product $product): void
    {
        $data = $request->validate([
            'product.name' => 'string|required',
            'product.price' => 'integer|required',
            'product.count' => 'integer|required',
            'product.user_id' => 'required|exists:users,id',
            'product.event_id' => 'required|exists:events,id',
        ]);

        $product->update($data['product']);

        Alert::success('Product update successfully.');
    }

    /**
     * Удаление продукта
     * @param Product $product
     * @return RedirectResponse
     */
    public function delete(Product $product): RedirectResponse
    {
        Product::query()->findOrFail($product->id)->delete();

        Alert::success('Product deleted successfully.');

        return redirect()->route('platform.products');
    }
}
