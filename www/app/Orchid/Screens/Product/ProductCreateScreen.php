<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Orchid\Layouts\Product\ProductCreateLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class ProductCreateScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     * @return string|null
     */
    public function name(): ?string
    {
        return 'ProductCreateScreen';
    }

    /**
     * The screen's action buttons.
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [Button::make('Create')->method('create')];
    }

    /**
     * The screen's layout elements.
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        /* todo: добавить отображение аттрибутов */
        return [
            ProductCreateLayout::class,
        ];
    }

    /**
     * Создание продукта
     * @param Request $request
     * @return void
     */
    public function create(Request $request): void
    {
        $data = $request->validate([
            'product.name' => 'string|required',
            'product.price' => 'integer|required',
            'product.count' => 'integer|required',
            'product.user_id' => 'required|exists:users,id',
            'product.event_id' => 'required|exists:events,id',
        ]);

        Product::query()->create($data['product']);

        Alert::success('Product created successfully.');
    }
}
