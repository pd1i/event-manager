<?php

namespace App\Orchid\Screens\Product\Attribute;

use App\Models\Attribute;
use App\Orchid\Layouts\Product\Attribute\AttributeCreateLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class AttributeCreateScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Attribute Create';
    }

    /**
     * The screen's action buttons.
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [Button::make('Create')->method('create')];
    }

    /**
     * The screen's layout elements.
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            AttributeCreateLayout::class
        ];
    }

    /**
     * Создание аттрибута продукта
     * @param Request $request
     * @return void
     */
    public function create(Request $request): void
    {
        $data = $request->validate([
            'attribute.name' => 'string|required',
            'attribute.description' => 'string|required',
            'attribute.attribute_type_id' => 'required|exists:attribute_types,id',
        ]);

        Attribute::query()->create($data['attribute']);

        Alert::success('Product attribute created successfully.');
    }
}
