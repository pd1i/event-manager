<?php

namespace App\Orchid\Screens\Product\Attribute;

use App\Models\Attribute;
use App\Orchid\Layouts\Product\Attribute\AttributeListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class AttributeListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     * @return array
     */
    public function query(): iterable
    {
        return [
            'attributes' => Attribute::query()->paginate(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Attribute List';
    }

    /**
     * The screen's action buttons.
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [Link::make('Add')->route('platform.products.attributes.create')];
    }

    /**
     * The screen's layout elements.
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            AttributeListLayout::class
        ];
    }
}
