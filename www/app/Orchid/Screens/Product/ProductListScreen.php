<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Orchid\Layouts\Product\ProductListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class ProductListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     * @return array
     */
    public function query(): iterable
    {
        return [
            'products' => Product::query()->paginate(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Products';
    }

    /**
     * The screen's action buttons.
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [Link::make('Add')->route('platform.products.create')];
    }

    /**
     * The screen's layout elements.
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [ProductListLayout::class];
    }

    /**
     * Удаление продукта
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        Product::query()->findOrFail($request->id)->delete();

        Alert::success('Product deleted successfully.');

        return redirect()->route('platform.products');
    }
}
