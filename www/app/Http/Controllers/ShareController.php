<?php

namespace App\Http\Controllers;

use App\Models\Share;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ShareController extends Controller
{
    /**
     * Создание пригласительной ссылки
     */
    public function create(Request $request): RedirectResponse
    {
        $createRequest = Share::create([
            'user_id' => $request->user()->id,
            'event_id' => $request->eventId,
            'code' => Str::uuid()
        ]);

        return back()->with('success', "Share link {$createRequest->code} created successfully.");
    }

    /**
     * Обработка пригласительной ссылки
     */
    public function get(Request $request): RedirectResponse
    {
        if (!isset($request->inviteCode)) {
            return back()->with('error', 'Invite code not found.');
        }

        $share = Share::query()->where('code', $request->inviteCode)->first();

        /**
         * Добавление пользователя в событие
         */
        $user = $request->user();
        $user->events()->attach([$share->event->id]);

        $user->roles()->attach(2, ['event_id' => $share->event->id]);

        /**
         * Удаление пригласительной ссылки
         */
        $share->delete();

        return back()->with('success', 'Share link adding successfully.');
    }
}
