<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Отображение главной страницы профиля
     * @return Factory|Application|View|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): Factory|Application|View|\Illuminate\Contracts\Foundation\Application
    {
        return view('profile.index', [
            'user' => auth()->user()
        ]);
    }

    /**
     * Обновление профиля
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'name' => 'max:255',
                'email' => 'max:255',
            ],
            [
                'name.required' => 'Name field is required.',
                'email.required' => 'Title field is required.',
            ]
        );

        $user = auth()->user();
        $user->update($validatedData);

        return back()->with('success', 'Profile update successfully.');
    }
}
