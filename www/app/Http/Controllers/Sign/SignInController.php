<?php

namespace App\Http\Controllers\Sign;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignInController extends Controller
{
    /**
     * Отображение страницы с авторизации
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('sign.in');
    }

    /**
     * Обработка авторизация в системе
     * @param Request $request
     * @return RedirectResponse
     */
    public function signIn(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'email' => 'required',
                'password' => 'required'
            ],
            [
                'email.required' => 'Email field is required.',
                'password.required' => 'Password field is required.'
            ]
        );

        if (!Auth::attempt($validatedData, $request->has('remember'))) {
            return back()->withErrors([
                'auth' => 'Incorrect email or password.',
            ]);
        };

        $request->session()->regenerate();

        return redirect()->intended(route('events.index', absolute: false));
    }
}
