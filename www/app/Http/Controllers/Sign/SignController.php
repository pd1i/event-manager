<?php

namespace App\Http\Controllers\Sign;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignController extends Controller
{
    /**
     * Обновление пароля
     * @param Request $request
     * @return RedirectResponse
     */
    public function updatePassword(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'old_password' => 'required',
                'new_password' => 'required',
            ],
            [
                'old_password.required' => 'OldPassword field is required.',
                'new_password.required' => 'NewPassword field is required.',
            ]
        );

        if (!Hash::check($validatedData['old_password'], $request->user()->password)) {
            return back()->with('error', 'Old password does not match.');
        }

        $request->user()->update([
            'password' => Hash::make($validatedData['new_password']),
        ]);

        return back()->with('success', 'Password update successfully.');
    }

    /**
     * Выход из системы
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->intended(route('home', absolute: false));
    }
}
