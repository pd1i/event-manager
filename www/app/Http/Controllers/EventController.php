<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Получение всех событий данного пользователя
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $user = auth()->user();

        return view('events.index', [
            'events' => $user->events ?? []
        ]);
    }

    /**
     * Получение детальное событий
     * @param Request $request
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function detail(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $query = Event::with(['users', 'products']);

        $query->where('id', $request->id);

        if (isset($request->user_id)) {
            $query->with('products', function ($q) use ($request) {
                $q->where('user_id', $request->user_id);
            });
        }

        if (isset($request->count_min)) {
            $query->with('products', function ($q) use ($request) {
                $q->where('count', '>', $request->count_min);
            });
        }

        if (isset($request->count_max)) {
            $query->with('products', function ($q) use ($request) {
                $q->where('count', '<', $request->count_max);
            });
        }

        $event = $query->get()->first();

        return view('events.detail', [
            'event' => $event,
            'users' => $event->users,
            'products' => $event->products
        ]);
    }

    /**
     * Создание нового event
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'title' => 'required'
            ],
            [
                'name.required' => 'Name field is required.',
                'title.required' => 'Title field is required.'
            ]
        );

        $createRequest = Event::create($validatedData);

        $user = $request->user();

        /**
         * Привязка события к пользователю
         */
        $user->events()->attach($createRequest->id);

        /**
         * Установка роли: 1 - создатель
         */
        $user->roles()->attach(1, ['event_id' => $createRequest->id]);

        return back()->with('success', 'Event created successfully.');
    }

    /**
     * Удаление event
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id): RedirectResponse
    {
        $this->authorize('delete', Event::class);

        $eventItem = Event::find($id);
        $eventItem->users()->detach();
        $eventItem->delete();

        return back()->with('success', 'Event delete successfully.');
    }
}
