<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Добавление новых продуктов
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'price' => 'required',
                'count' => 'required'
            ],
            [
                'name.required' => 'Name field is required.',
                'price.required' => 'Title field is required.',
                'count.required' => 'Title field is required.'
            ]
        );

        $user = $request->user();

        $event = Event::find($request->eventId);
        $event->products()->create([
            'name' => $validatedData['name'],
            'price' => $validatedData['price'],
            'count' => $validatedData['count'],
            'user_id' => $user->id
        ]);

        return back()->with('success', 'Product created successfully.');
    }

    /**
     * Обновление продуктов
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'price' => 'required',
                'count' => 'required'
            ],
            [
                'name.required' => 'Name field is required.',
                'price.required' => 'Title field is required.',
                'count.required' => 'Title field is required.'
            ]
        );

        $product = Product::find($request->productId);
        $product->update($validatedData);

        return back()->with('success', 'Product update successfully.');
    }

    /**
     * Удаление продуктов
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $product = Product::find($request->productId);
        $product->delete();

        return back()->with('success', 'Product delete successfully.');
    }
}
